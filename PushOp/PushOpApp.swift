//
//  PushOpApp.swift
//  PushOp
//
//  Created by Ailson Cordeiro Filho on 02/02/21.
//

import SwiftUI

@main
struct PushOpApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
