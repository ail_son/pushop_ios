//
//  GradientBackgroundSyleButton.swift
//  PushOp
//
//  Created by Ailson Cordeiro Filho on 02/02/21.
//

import Foundation
import SwiftUI

struct GradientButtonStyle: ButtonStyle {
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(minWidth: 0, maxWidth: 350)
            .foregroundColor(Color.white)
            .padding()
            .background(LinearGradient(gradient: Gradient(colors: [Color.yellow, Color.orange]), startPoint: .leading, endPoint: .trailing))
            .cornerRadius(40.0)
            .font(.title)
    }
}

