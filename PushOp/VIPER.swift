//
//  Interfaces.swift
//  VIPER
//
//  Created by Tibor Bödecs on 2019. 05. 16..
//  Copyright © 2019. Tibor Bödecs. All rights reserved.
//

import Foundation

// MARK: - VIPER

public protocol RouterInterface: RouterToPresenterInterface {
    associatedtype PresenterRouter

    var presenter: PresenterRouter! { get set }
}

public protocol InteractorInterface: InteractorToPresenterInterface {
    associatedtype PresenterInteractor
    
    var presenter: PresenterInteractor! { get set }
}

public protocol PresenterInterface: PresenterToRouterInterface & PresenterToInteractorInterface & PresenterToViewInterface {
    associatedtype RouterPresenter
    associatedtype InteractorPresenter
    /*associatedtype ViewPresenter*/

    var router: RouterPresenter! { get set }
    var interactor: InteractorPresenter! { get set }
    /*var view: ViewPresenter! { get set }*/
}

public protocol ViewInterface/*: ViewPresenterInterface*/ {
    associatedtype PresenterView
    
    var presenter: PresenterView! { get set }
}

public protocol EntityInterface {
    
}

// MARK: - "i/o" transitions

public protocol RouterToPresenterInterface: class {
    
}

public protocol InteractorToPresenterInterface: class {
    
}

public protocol PresenterToRouterInterface: class {
    
}

public protocol PresenterToInteractorInterface: class {
    
}

public protocol PresenterToViewInterface: class {
    
}

// MARK: - module

public protocol ModuleInterface {

    associatedtype ViewViper where ViewViper: ViewInterface
    associatedtype Presenter where Presenter: PresenterInterface
    associatedtype Router where Router: RouterInterface
    associatedtype Interactor where Interactor: InteractorInterface
    
    func assemble(/*view: View, */presenter: Presenter, router: Router, interactor: Interactor)
}

public extension ModuleInterface {

    func assemble(presenter: Presenter, router: Router, interactor: Interactor) {
        presenter.interactor = (interactor as! Self.Presenter.InteractorPresenter)
        presenter.router = (router as! Self.Presenter.RouterPresenter)
        
        interactor.presenter = (presenter as! Self.Interactor.PresenterInteractor)
        
        router.presenter = (presenter as! Self.Router.PresenterRouter)
    }
}
