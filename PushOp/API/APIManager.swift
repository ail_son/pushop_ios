//
//  APIManager.swift
//  PushOp
//
//  Created by Ailson Cordeiro Filho on 03/02/21.
//

import Foundation

class APIManager {
    static let shared = { APIManager() }()
    
    lazy var baseURL: String = {
        return "https://pushop.getsandbox.com:443/"
    }()
}
