//
//  LoginRouter.swift
//  PushOp
//
//  Created by Ailson Cordeiro Filho on 03/02/21.
//

import Foundation
import SwiftUI

final class LoginRouter: RouterInterface {

    weak var presenter: LoginPresenterToRouterInterface!
    
    weak var viewController: UIViewController?
}

extension LoginRouter: LoginRouterToPresenterInterface {
    func pushToTodo() {
        
    }
}
