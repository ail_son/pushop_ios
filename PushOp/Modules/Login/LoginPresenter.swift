//
//  LoginPresenter.swift
//  PushOp
//
//  Created by Ailson Cordeiro Filho on 02/02/21.
//

import Foundation
import SwiftUI
import Combine

final class LoginPresenter: PresenterInterface {
    var router: LoginRouterToPresenterInterface!
    var interactor: LoginInteractorToPresenterInterface!
    weak var viewModel: LoginViewModel!
}

extension LoginPresenter: LoginPresenterToRouterInterface {

}

extension LoginPresenter: LoginPresenterToInteractorInterface {

}

extension LoginPresenter: LoginPresenterToViewInterface {
    func login(usuario: String, senha: String){
        self.viewModel.carregando = true
        let entitty = LoginEntity(id: 0, usuario: usuario, senha: senha)
        self.interactor.login(entidade: entitty) { (entidade) in
            self.viewModel.carregando = false
            self.viewModel.login = entidade
            self.viewModel.logado = true
        } failure: { (mensagem) in
            self.viewModel.carregando = false
            self.viewModel.erro = true
            self.viewModel.mensagem = mensagem
        }
    }
}
