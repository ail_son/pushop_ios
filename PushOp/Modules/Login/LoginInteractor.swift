//
//  LoginInteractor.swift
//  PushOp
//
//  Created by Ailson Cordeiro Filho on 02/02/21.
//

import Foundation
import Combine

final class LoginInteractor: InteractorInterface {
    weak var presenter: LoginPresenterToInteractorInterface!
}

extension LoginInteractor: LoginInteractorToPresenterInterface {
    func login (entidade: LoginEntity,
                                   success: @escaping (LoginEntity) -> (),
                                   failure: @escaping (String) -> ()) {
        let client = APIClient.shared
        
        client.login(endPoint: EndPoints.login, entidade: entidade) { (code, entidade) in
            success(entidade)
        } failure: { (code) in
            if code == 201 {
                failure("Usuário ou senha incorretos!")
            } else {
                failure("Ocorreu um erro!")
            }
            
        }
    }
}
