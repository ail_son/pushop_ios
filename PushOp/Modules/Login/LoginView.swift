//
//  LoginView.swift
//  PushOp
//
//  Created by Ailson Cordeiro Filho on 02/02/21.
//

import SwiftUI

struct LoginView:  ViewInterface, View {
    var presenter: LoginPresenterToViewInterface!
    
    @EnvironmentObject var env: LoginEnvironment
    @ObservedObject var viewModel: LoginViewModel
    @State private var usuario = ""
    @State private var senha = ""
    
    let spinnerConf = SpinnerConfiguration()
    let spinner = SpinnerView()
    
    var body: some View {
        if !self.$viewModel.logado.wrappedValue {
            ZStack {
                Image("splash_background")
                    .resizable()
                    .edgesIgnoringSafeArea(.all)
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                VStack{
                    Image("splash")
                    TextField("Login", text: $usuario)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .padding()
                    SecureField("Senha", text: $senha)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .padding()
                    Button(action: {
                        self.presenter.login(usuario: self.usuario, senha: self.senha)
                    }) {
                        Text("Entrar")
                            .fontWeight(.bold)
                            .font(.title)
                    }.buttonStyle(GradientButtonStyle())
                    .alert(isPresented: $viewModel.erro, content: {
                        Alert(title: Text($viewModel.mensagem.wrappedValue))
                    })
                    
                }
                if(self.$viewModel.carregando.wrappedValue) {
                    SpinnerView()
                }
            }
        } else {
            NavigationView {
                TodoModule().build(entidade: LoginEntity(id: 0, usuario: "teste", senha: "bin"))
            }
        }
    }
}

#if DEBUG
struct Login_Previews: PreviewProvider {
    static var previews: some View {
        let presenter = LoginPresenter()
        let viewModel = LoginViewModel()
        presenter.viewModel = viewModel
        return LoginView(presenter: presenter,
                        viewModel: viewModel)
        .environmentObject(LoginEnvironment())
    }
}
#endif
