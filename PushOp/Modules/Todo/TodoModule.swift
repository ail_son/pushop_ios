//
//  TaskModule.swift
//  VIPERAndSwiftUI
//
//  Created by Tibor Bödecs on 2019. 09. 12..
//  Copyright © 2019. Tibor Bödecs. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI
import Combine

// MARK: - router

protocol TodoRouterPresenterInterface: RouterToPresenterInterface {

}

// MARK: - presenter

protocol TodoPresenterRouterInterface: PresenterToRouterInterface {

}

protocol TodoPresenterInteractorInterface: PresenterToInteractorInterface {
    
}

protocol TodoPresenterViewInterface: PresenterToViewInterface {
    func fetch()
    func newTodo(with title: String)
    func delete(at offsets: IndexSet)
    func toggle(todo: TodoEntity)
}

// MARK: - interactor

protocol TodoInteractorPresenterInterface: InteractorToPresenterInterface {
    func requestPublisher() -> AnyPublisher<[TodoEntity], HTTP.Error>
}

// MARK: - module builder

final class TodoModule: ModuleInterface {
    typealias ViewViper = TodoView
    typealias Presenter = TodoPresenter
    typealias Router = TodoRouter
    typealias Interactor = TodoInteractor

    func build(entidade: LoginEntity) -> some View {
        let presenter = Presenter()
        presenter.entidade = entidade
        let interactor = Interactor()
        let router = Router()

        let viewModel = TodoViewModel()
        let view = ViewViper(presenter: presenter, viewModel: viewModel)
            .environmentObject(TodoEnvironment())
        presenter.viewModel = viewModel

        self.assemble(presenter: presenter, router: router, interactor: interactor)
        
        return view
    }
}
