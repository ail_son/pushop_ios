//
//  ContentView.swift
//  PushOp
//
//  Created by Ailson Cordeiro Filho on 02/02/21.
//
import SwiftUI

struct ContentView: View {
    @EnvironmentObject var env: LoginEnvironment
    var body: some View {
//        NavigationView {
//            if(env.userAuth == false){
                LoginModule().build()
//            } else {
//                TodoModule().build()
//            }
//        }
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    return ContentView()
  }
}
#endif
